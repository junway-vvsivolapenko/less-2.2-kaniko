FROM python:3.8-alpine AS compile

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN apk add --no-cache postgresql-dev libffi-dev musl-dev gcc

RUN python -m venv /opt/venv \
    && python -m pip install --upgrade pip

ENV PATH="/opt/venv/bin:$PATH"

ADD ./requirements/requirements.txt .

RUN pip install -r requirements.txt --no-cache-dir


FROM python:3.8-alpine

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

ARG UID=1000
ARG GID=1000

RUN apk add --no-cache postgresql-libs

RUN addgroup -g $GID -S django && adduser -u $UID -S django -G django

WORKDIR /home/django/app

COPY . .

COPY --from=compile /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"

USER django

ENTRYPOINT ["./entrypoint.sh"]
