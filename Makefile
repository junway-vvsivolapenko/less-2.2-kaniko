all: help

define HELP_MESSAGE =
	USAGE:
	build - build main project
	up - start containers
	down - stop containers
	clean - remove all dangling images
	restart-web - restart django service
	test - start all tests
endef

PROJECT = compose/docker-compose.yml
PROJECT_NAME = junway-vvsivolapenko-web
DJANGO_ROOT = django-blog-v-2
DJANGO_CONTAINER_NAME = junway-web
UID = $(shell id -u)
GID = $(shell id -g)
OS := $(shell uname)

help:
	$(info $(HELP_MESSAGE))

build:
ifeq ($(OS), Darwin)
	$(shell mkdir -p $(DJANGO_ROOT)/media && mkdir -p $(DJANGO_ROOT)/static)
	docker-compose -f $(PROJECT) -p $(PROJECT_NAME) build --build-arg UID=1000 --build-arg GID=1000
else
	$(shell mkdir -p $(DJANGO_ROOT)/media && mkdir -p $(DJANGO_ROOT)/static)
	docker-compose -f $(PROJECT) -p $(PROJECT_NAME) build --build-arg UID=$(UID) --build-arg GID=$(GID)
endif

up:
	docker-compose -f $(PROJECT) -p $(PROJECT_NAME) up -d

down:
	docker-compose -f $(PROJECT) -p $(PROJECT_NAME) down

restart-web:
	docker-compose -f $(PROJECT) -p $(PROJECT_NAME) restart $(DJANGO_CONTAINER_NAME)

test:
	docker exec -ti $(DJANGO_CONTAINER_NAME) pytest -p no:cacheprovider -n auto --flake8 --cov .

clean:
	docker rmi -f $(shell docker images -f "dangling=true" -q)
