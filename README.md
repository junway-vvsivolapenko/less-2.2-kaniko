## Kaniko

During the development of the gitlab-ci.yml, the [Gitlab official documentation](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) was used.

gitlab-runner runinig in unprivileged mode:

```config.toml
[[runners]]
  name = "junway docker runner"
  url = "https://gitlab.com/"
  token = "NiceTryBuddy"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:19.03.12"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```

